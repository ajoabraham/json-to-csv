package com.vero.wrangler.common;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

public final class CSVWriter implements Closeable {
	private CSVPrinter csvPrinter = null;
	
	private boolean isHeaderPrinted = false;
	private List<String> headerNames = null;
	
	public CSVWriter(String fileName) throws IOException {
		csvPrinter = createCSVPrinter(new File(fileName));
		headerNames = new ArrayList<>();
	}
	
	public void printRecord(Map<String, String> record) throws IOException {
		if (!isHeaderPrinted) {			
			csvPrinter.printRecord(headerNames);
			isHeaderPrinted = true;
		}
		
		List<String> values = new ArrayList<>();
		for (String headerName : headerNames) {
			values.add(record.get(headerName));
		}

		csvPrinter.printRecord(values);
	}
	
	public void setHeaderNames(List<String> headerNames) {
		this.headerNames = headerNames;
	}
	
	public List<String> getHeaderNames() {
		return headerNames;
	}

	@Override
	public void close() throws IOException {		
		if (csvPrinter != null) {
			csvPrinter.close();
		}
	}
		
	private static CSVPrinter createCSVPrinter(File file) throws IOException {
		return CSVFormat.newFormat('|').withQuote('"')
				.withRecordSeparator(System.lineSeparator())
				.print(new PrintWriter(file, StandardCharsets.UTF_8.name()));	
	}
}
