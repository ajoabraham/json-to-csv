package com.vero.wrangler.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public final class JsonFlattener {
	private ObjectMapper objectMapper = null;
	
	public JsonFlattener() {
		objectMapper = new ObjectMapper();
	}
	
	public Map<String, String> parseAsNode(JsonNode jsonNode) {
        Map<String, String> flatJson = new HashMap<>();
        flattenNode(jsonNode, flatJson, "");
        return flatJson;
    }

    public List<Map<String, String>> parseAsArray(JsonNode jsonArray) {
        List<Map<String, String>> flatJson = new ArrayList<Map<String, String>>();
        for (Iterator<JsonNode> i = jsonArray.elements(); i.hasNext();) {
            JsonNode jsonObject = i.next();
            Map<String, String> stringMap = parseAsNode(jsonObject);
            flatJson.add(stringMap);
        }
        
        return flatJson;
    }

    public List<Map<String, String>> parseJson(String json) throws Exception {
    	JsonNode jsonNode = objectMapper.readTree(json);
    	List<Map<String, String>> flatJson = null;
    	
    	if (jsonNode.isArray()) {
    		if(jsonNode.elements().next().isArray()){
    			flatJson = parseAsArrayOfArrays(jsonNode,new ArrayList<Map<String, String>>());
    		}else{
    			flatJson =  parseAsArray(jsonNode);
    		}
    	}
    	else {
    		flatJson = new ArrayList<Map<String, String>>();
            flatJson.add(parseAsNode(jsonNode));
    	}

        return flatJson;
    }

    private List<Map<String, String>> parseAsArrayOfArrays(JsonNode jsonArray,List<Map<String, String>> flatJson) {
        for (Iterator<JsonNode> i = jsonArray.elements(); i.hasNext();) {
            JsonNode jsonObject = i.next();
            if(jsonObject.isArray()){
            	parseAsArrayOfArrays(jsonObject,flatJson);
            }else{
            	Map<String, String> stringMap = parseAsNode(jsonObject);
                flatJson.add(stringMap);
            }
        }
        
        return flatJson;
	}

	private void flattenArray(JsonNode obj, Map<String, String> flatJson, String prefix) {
    	int count = 0;
        for (Iterator<JsonNode> i = obj.elements(); i.hasNext();) {
        	JsonNode node = i.next();
        	
            if (node.isArray()) {
                if (node.size() < 1) continue;
                flattenArray(node, flatJson, prefix + count++);
            } 
            else if (node.isObject()) {
                flattenNode(node, flatJson, prefix + count++);
            } 
            else {
                String value = node.asText();
                if (value != null)
                    flatJson.put(prefix + count++, value);
            }
        }
    }

    private void flattenNode(JsonNode obj, Map<String, String> flatJson, String prefix) {
        for (Iterator<Map.Entry<String, JsonNode>> i = obj.fields(); i.hasNext();) {
        	Map.Entry<String, JsonNode> entry = i.next();
        	String fieldName = entry.getKey();
        	JsonNode node = entry.getValue();
        	
            if (node.isObject()) {
                flattenNode(node, flatJson, generatePrefix(prefix, fieldName));
            } 
            else if (node.isArray()) {
                if (node.size() < 1) continue;
                flattenArray(node, flatJson, generatePrefix(prefix, fieldName));
            } 
            else {
                String value = node.asText();
                if (value != null && !value.equals("null"))
                    flatJson.put(generatePrefix(prefix, fieldName), value);
            }
        }
    }
    
    private String generatePrefix(String prefix, String fieldName) {
    	boolean isPrefixBlank = prefix == null || prefix.trim().equals("");
    	boolean isFieldNameBlank = fieldName == null || fieldName.trim().equals("");
    	
    	String newPrefix = "";

    	if (isPrefixBlank && !isFieldNameBlank) {
    		newPrefix = fieldName;
    	}
    	else if (!isPrefixBlank && !isFieldNameBlank) {
    		newPrefix = prefix + "_" + fieldName;
    	}
    	else if (!isPrefixBlank && isFieldNameBlank) {
    		newPrefix = prefix;
    	}
    	
    	return newPrefix;
    }
}
