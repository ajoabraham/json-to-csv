package com.vero.wrangler.common;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

public final class MixpanelJQLReader {
	private static final String apiSecret = "584c3903fb39b2a528fbefd1fb4f41df";
	private static final String password = "";
	private static final String url = "https://mixpanel.com/api/2.0/jql";
	private static final String fromDate = "2016-01-01";
	private static final String toDate = "2016-04-27";
	private static final String jql = "strictOrder.js";
	private static final String csvFileName = "mixpanel_output.csv";
	private static final String tempFileName = "temp_json_output.txt";
	private static final Charset encoding = StandardCharsets.UTF_8;
	
	private JsonFlattener jsonFlattener = null;
	
	public MixpanelJQLReader() {
		jsonFlattener = new JsonFlattener();
	}
	
	public void readData() throws IOException {	
		String jqlFile = IOUtils.toString(getClass().getResourceAsStream("/"+jql));
		ObjectNode jqlParams = JsonNodeFactory.instance.objectNode();
		jqlParams.put("from_date", fromDate);
		jqlParams.put("to_date", toDate);
		
		CredentialsProvider provider = new BasicCredentialsProvider();
		UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(apiSecret, password);
		provider.setCredentials(AuthScope.ANY, credentials);
		
		try (CloseableHttpClient client = HttpClientBuilder.create().setDefaultCredentialsProvider(provider).build();
				CSVWriter writer = new CSVWriter(csvFileName)) {
			
    		HttpPost httpPost = new HttpPost(url);
    		
    		List<NameValuePair> params = new ArrayList<>();
    		
    		params.add(new BasicNameValuePair("params", jqlParams.toString()));
    		params.add(new BasicNameValuePair("script",  jqlFile));
    		
    		System.out.print(params);
    		
    		httpPost.setEntity(new UrlEncodedFormEntity(params));
    		
    		CloseableHttpResponse response = client.execute(httpPost);
    		
    		if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
    			try (BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), StandardCharsets.UTF_8));
    					PrintWriter tempOut = new PrintWriter(new File(tempFileName), encoding.name())) {    			
        			String json = null;
        			
        			List<String> headerNames = new ArrayList<>();
        			int lineCount = 0;
        			while ((json = in.readLine()) != null) {
        				System.out.print(json);
        				if (!json.trim().equals("")) {
        					if (lineCount++ == 0) {
        						System.out.print("[");
        					}
        					else {
        						System.out.println(",");
        					}
        					
        					System.out.print(json);
        					
        					List<Map<String, String>> records = jsonFlattener.parseJson(json);
        					for (Map<String, String> record : records) {
        						headerNames.addAll(record.keySet().stream().filter(k -> !headerNames.contains(k)).collect(Collectors.toList()));
        					}
        					
        					tempOut.println(json);
        				}
        			}
        			
        			System.out.println("]");
        			
        			writer.setHeaderNames(headerNames);
        			
        			System.out.println("Processed " + lineCount + " records");
    			}
    			
    			System.out.println("Generating CSV file...");
    			
    			try (BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(tempFileName)))) {
    				String json = null;
    				
    				while ((json = in.readLine()) != null) {
    					List<Map<String, String>> records = jsonFlattener.parseJson(json);
    					
    					for (Map<String, String> record : records) {
    						writer.printRecord(record);
    					}
    				}
    			}
    			
    			System.out.println("Successfully generated CSV file");
    			
    		}
    		else {
    			System.out.println("Invalid status code - " + response.getStatusLine().getStatusCode());
    			System.out.println("Invalid status code - " + EntityUtils.toString(response.getEntity()));
    		}    		
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		try {
    		MixpanelJQLReader reader = new MixpanelJQLReader();
    		reader.readData();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
