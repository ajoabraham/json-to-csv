function main() {
  return Events({
    from_date: params.from_date,
    to_date:   params.to_date
  })
  .groupByUser(function(state,events){
    state= state || {search:[]};
    var last_search;
    if(state.search.length>1){
      if(state.search[state.search.length-1].search_state=="open"){
      	last_search = state.search[state.search.length-1];
      }
    }
    for(var i=0;i<events.length;i++){
      	var e = events[i];
      	if(isSearchBarLoadedEvent(e)){
      		continue;
      	}

	    if(isValidSearchEvent(e)){
			if(typeof last_search == "undefined"){
				 last_search ={
					distinct_id: e.distinct_id,
				    search_time: formatDate(e.time),
					search_event: e.name.toLowerCase(),			   
				    search_term: e.properties.Search,
				    search_state: "open",visits:0 };
				 state.search.push(last_search);
			}else{
				if(isOpen(last_search)){
				 last_search.search_state="failed";
				}
				 
			 	var new_search = {
			 	distinct_id: e.distinct_id,
			    search_time: formatDate(e.time),
				search_event: e.name.toLowerCase(),			   
			    search_term: e.properties.Search,
			    search_state: "open",visits:0 };
			    
				state.search.push(new_search);
				last_search = new_search;
			}
	    }else if(isVisit(e)){
			 if(isOpen(last_search)){
			 	last_search.visits++;
			 }
		}else if(isAssetAction(e)){
			
			if(isOpen(last_search)){
				 last_search.search_state = "succeeded";
				 last_search.action= e.properties.action;
				 last_search.action_time = formatDate(e.time);
				 last_search.number_on_page= e.properties.number_on_page;
				 last_search.page_number= e.properties.page_number;
			 }
		}else{
			if(isOpen(last_search)){
				last_search.search_state = "failed";
			   	last_search.intervening_event = e.name.toLowerCase();				
			}			
		}
	}
		
	return state;
  })
  .filter(function(item){ return item.value.search.length > 0;})
  .map(function(item){ return item.value.search });
  
  function isOpen(search){
    return typeof search != "undefined" && search.search_state==="open";
  }
  function isValidSearchEvent(event){
    return event.name.toLowerCase().indexOf("executed search")>-1
              && typeof event.properties.Search != "undefined";
  }
  function isSearchBarLoadedEvent(event){
  	return event.name.toLowerCase().indexOf("executed search")>-1
              && typeof event.properties.Search == "undefined";
  }
  function isVisit(event){
    return event.name.toLowerCase().indexOf("visit")>-1;
  }
  function isAssetAction(event){
   return event.name.toLowerCase().indexOf("asset action")>-1 ;
  }
  function formatDate(time){
	  return time/1000;
  }
}